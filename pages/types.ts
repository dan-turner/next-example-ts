import { NextContext } from "next";
import { MyStore } from "../configureStore";

export type MyNextContext = NextContext & {
    isServer: boolean;
    store: MyStore;
}
