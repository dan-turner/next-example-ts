import React from 'react'
import { connect } from 'react-redux'

import { loadData, startClock, tickClock, StartClockAction } from '../reducers/actions'
import Page from '../components/page'
import { AppState } from '../reducers';
import { MyNextContext } from './types';
import { ActionCreator } from 'redux';

type OwnProps = {};

type MapStateToProps = {
};

type MapDispatchToProps = {
  onStartClock: ActionCreator<StartClockAction>;
};

type Props = MapStateToProps & MapDispatchToProps & OwnProps;

class Index extends React.Component<Props> {
  static async getInitialProps ({ store, isServer }: MyNextContext) {
    store.dispatch(tickClock(isServer))

    if (!store.getState().placeholderData) {
      store.dispatch(loadData())
    }

    return { isServer }
  }

  componentDidMount () {
    this.props.onStartClock();
  }

  render () {
    return <Page title='Index Page' linkTo='/other' NavigateTo='Other Page' />
  }
}

export default connect<MapStateToProps, MapDispatchToProps, OwnProps, AppState>(
  undefined,
  { onStartClock: startClock }
)(Index);
