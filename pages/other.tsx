import React from 'react'
import { connect } from 'react-redux'

import { startClock, tickClock, StartClockAction } from '../reducers/actions'
import Page from '../components/page'
import { AppState } from '../reducers';
import { MyNextContext } from './types';
import { ActionCreator } from 'redux';

type MapDispatchToProps = {
  onStartClock: ActionCreator<StartClockAction>;
};

type Props = MapDispatchToProps;

class Other extends React.Component<Props> {
  static async getInitialProps ({ store, isServer }: MyNextContext) {
    store.dispatch(tickClock(isServer))
    return { isServer }
  }

  componentDidMount () {
    this.props.onStartClock();
  }

  render () {
    return <Page title='Other Page' linkTo='/' NavigateTo='Index Page' />
  }
}

export default connect<undefined, MapDispatchToProps, undefined, AppState>(
  undefined,
  { onStartClock: startClock }
)(Other);

