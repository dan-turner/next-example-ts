import Link from 'next/link'
import { connect } from 'react-redux'

import Counter from './counter'
import Clock from './clock'
import { AppState } from '../reducers';

type OwnProps = {
  title: string;
  linkTo: string;
  NavigateTo: string;
};

type MapStateToProps = {
  error: boolean;
  lastUpdate: number;
  light: boolean;
  placeholderData: any;
};

type MapDispatchToProps = {
};

type Props = MapStateToProps & MapDispatchToProps & OwnProps;

const Page = ({
  error,
  lastUpdate,
  light,
  linkTo,
  NavigateTo,
  placeholderData,
  title
}: Props) => {
  return (
    <div>
      <h1>{title}</h1>
      <Clock lastUpdate={lastUpdate} light={light} />
      <Counter />
      <nav>
        <Link href={linkTo}>
          <a>Navigate: {NavigateTo}</a>
        </Link>
      </nav>
      {placeholderData && (
        <pre>
          <code>{JSON.stringify(placeholderData, null, 2)}</code>
        </pre>
      )}
      {error && <p style={{ color: 'red' }}>Error: {error}</p>}
    </div>
  )
}

export default connect<MapStateToProps, MapDispatchToProps, OwnProps, AppState>(
  ({ error, lastUpdate, light, placeholderData }) => ({ error, lastUpdate, light, placeholderData })
)(Page);
