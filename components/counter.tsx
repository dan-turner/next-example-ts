import React from 'react'
import { connect } from 'react-redux'

import { increment, decrement, reset, IncrementAction, DecrementAction, ResetAction } from '../reducers/actions'
import { ActionCreator } from 'redux';
import { AppState } from '../reducers';

type OwnProps = {
};

type MapStateToProps = {
  count: number
};

type MapDispatchToProps = {
  onIncrement: ActionCreator<IncrementAction>;
  onDecrement: ActionCreator<DecrementAction>;
  onReset: ActionCreator<ResetAction>;
};

type Props = MapStateToProps & MapDispatchToProps & OwnProps;


const Counter = ({
  count,
  onIncrement,
  onDecrement,
  onReset
}: Props) => {
    return (
      <div>
        <h1>
          Count: <span>{count}</span>
        </h1>
        <button onClick={onIncrement}>+1</button>
        <button onClick={onDecrement}>-1</button>
        <button onClick={onReset}>Reset</button>
      </div>
    );
}

export default connect<MapStateToProps, MapDispatchToProps, OwnProps, AppState>(
  ({ count }) => ({ count }),
  { onIncrement: increment, onDecrement: decrement, onReset: reset }
)(Counter);
