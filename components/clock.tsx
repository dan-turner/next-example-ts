import React from 'react'
import styled from 'styled-components';

const pad = (n: number) => (n < 10 ? `0${n}` : n)

const format = (t : Date) => {
  const hours = t.getUTCHours()
  const minutes = t.getUTCMinutes()
  const seconds = t.getUTCSeconds()
  return `${pad(hours)}:${pad(minutes)}:${pad(seconds)}`
}

type Props = {
  lastUpdate: number;
  light: boolean;
};

const Container = styled.div<{ light: boolean }>`
  padding: 15px;
  display: inline-block;
  color: #82fa58;
  font: 50px menlo, monaco, monospace;
  background-color: ${props => props.light ? "#999" : "#000" };
`;

const Clock = ({
  lastUpdate,
  light
}: Props) => {
  return (
    <Container light={light}>
      {format(new Date(lastUpdate))}
    </Container>
  )
}

export default Clock;
