import { Action } from "redux";

export const actionTypes = {
  FAILURE: 'FAILURE',
  INCREMENT: 'INCREMENT',
  DECREMENT: 'DECREMENT',
  RESET: 'RESET',
  LOAD_DATA: 'LOAD_DATA',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  START_CLOCK: 'START_CLOCK',
  TICK_CLOCK: 'TICK_CLOCK'
}

export enum Actions {
  FAILURE= 'FAILURE',
  INCREMENT= 'INCREMENT',
  DECREMENT= 'DECREMENT',
  RESET= 'RESET',
  LOAD_DATA= 'LOAD_DATA',
  LOAD_DATA_SUCCESS= 'LOAD_DATA_SUCCESS',
  START_CLOCK= 'START_CLOCK',
  TICK_CLOCK= 'TICK_CLOCK'
}

export type FailureAction = Action<Actions.FAILURE> & {
  error: string
};
export type IncrementAction = Action<Actions.INCREMENT>;
export type DecrementAction = Action<Actions.DECREMENT>;
export type ResetAction = Action<Actions.RESET>;
export type LoadDataAction = Action<Actions.LOAD_DATA>;
export type LoadDataSuccessAction = Action<Actions.LOAD_DATA_SUCCESS> & {
  data: any
};
export type StartClockAction = Action<Actions.START_CLOCK>;
export type TickClockAction = Action<Actions.TICK_CLOCK> & {
  light: boolean,
  ts: number
};

export const failure = (error: string) : FailureAction => ({
  type: Actions.FAILURE,
  error
});

export const increment = () : IncrementAction => {
  return { type: Actions.INCREMENT }
}

export const decrement = () : DecrementAction => {
  return { type: Actions.DECREMENT }
}

export const reset = () : ResetAction => {
  return { type: Actions.RESET }
}

export const loadData = () : LoadDataAction => {
  return { type: Actions.LOAD_DATA }
}

export const loadDataSuccess = (data: any) : LoadDataSuccessAction => {
  return {
    type: Actions.LOAD_DATA_SUCCESS,
    data
  }
}

export const startClock = () : StartClockAction => {
  return { type: Actions.START_CLOCK }
}

export const tickClock = (isServer: boolean) : TickClockAction => {
  return {
    type: Actions.TICK_CLOCK,
    light: !isServer,
    ts: Date.now()
  }
}
