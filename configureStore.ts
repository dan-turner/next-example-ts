import { applyMiddleware, createStore, Store } from 'redux'
import createSagaMiddleware, { Task } from 'redux-saga'

import rootReducer, { InitialState, AppState } from './reducers'
import rootSaga from './sagas'

export type MyStore = Store<AppState> & {
  sagaTask: Task
};

const bindMiddleware = (middleware: any) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension')
    return composeWithDevTools(applyMiddleware(...middleware))
  }
  return applyMiddleware(...middleware)
}

export default (initialState = InitialState): MyStore => {
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(
    rootReducer,
    initialState,
    bindMiddleware([sagaMiddleware])
  )

  const sagaTask = sagaMiddleware.run(rootSaga)

  return {
    ...store,
    sagaTask
  }
};